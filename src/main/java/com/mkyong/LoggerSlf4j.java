package com.mkyong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerSlf4j {
    private static final Logger logger = LoggerFactory.getLogger(LoggerSlf4j.class);

    public LoggerSlf4j() {
        logger.debug("Test logger without annotation @Slf4j");
    }    
}