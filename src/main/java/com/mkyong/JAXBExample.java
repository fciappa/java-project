/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mkyong;

import com.mkyong.utils.Customer;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Francesco Ciappa <f.ciappa@gmail.com>
 */
@Slf4j
public class JAXBExample {

    public JAXBExample(String returnType) {
        try {
            File file = new File("R:\\file.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Customer.class);
            
            Customer customer = new Customer();
            if (returnType.equalsIgnoreCase("xml")) {
                customer.setId(100);
                customer.setName("mkyong");
                customer.setAge(29);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                jaxbMarshaller.marshal(customer, file);
                jaxbMarshaller.marshal(customer, System.out);
            } else if (returnType.equalsIgnoreCase("object")) {
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		customer = (Customer) jaxbUnmarshaller.unmarshal(file);
                log.debug("Customer object {}", customer);
            }
        } catch (JAXBException e) {
            log.error("Error on JAXB example", e);
        }

    }
}

