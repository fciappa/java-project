/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mkyong.swing;

/**
 *
 * @author Francesco Ciappa <f.ciappa@gmail.com>
 */

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import lombok.extern.slf4j.Slf4j;

@SuppressWarnings("serial")
@Slf4j
public class ConfirmDialogFrame {

    public ConfirmDialogFrame() {
        Frame cdframe = new Frame();
        ImageIcon icon = new ImageIcon("src/images/girl64.png");
        String[] options = {"Mary", "Nora", "Anna", "Lauren"};
        int x = JOptionPane.showOptionDialog(cdframe, "Lauren's mom had four kids: Maria, Martha, Margaret...",
                "The missing kid", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, icon, options, options[0]);
        log.info("Choice: [{}]", x);
        if (x == 3) {
            cdframe.getContentPane().add(cdframe.panel());
            cdframe.repaint();
            cdframe.revalidate();
            JOptionPane.showMessageDialog(null, "Close!");
        } else {
            cdframe.dispose();
            JOptionPane.showMessageDialog(null, "Nooope!");
        }
    }

    
}
