/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mkyong.swing;

/**
 *
 * @author Francesco Ciappa <f.ciappa@gmail.com>
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class Frame extends JFrame {
    public Frame() {
        getContentPane().setBackground(new Color(238, 232, 170));
        getContentPane().setLayout(null);
        setTitle("Confirm Dialog in Frame");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(false);
        setResizable(false);
        setSize(450, 300);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width / 2 - this.getWidth() / 2, dim.height / 2 - this.getHeight() / 2);
    }
    
    public JPanel panel() {

        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 444, 271);
        panel.setBackground(new Color(176, 224, 230));
        getContentPane().add(panel);
        panel.setLayout(null);

        JLabel lblIcon = new JLabel("");
        lblIcon.setBounds(30, 30, 200, 200);
        lblIcon.setIcon(new ImageIcon("src/images/girl200.png"));
        panel.add(lblIcon);

        JLabel lblText2 = new JLabel("Lauren is the 4th daughter!");
        lblText2.setVerticalAlignment(SwingConstants.TOP);
        lblText2.setFont(new Font("Tahoma", Font.ITALIC, 14));
        lblText2.setHorizontalAlignment(SwingConstants.CENTER);
        lblText2.setBounds(240, 130, 175, 100);
        panel.add(lblText2);

        JLabel lblText1 = new JLabel("Yaaaay!");
        lblText1.setHorizontalAlignment(SwingConstants.CENTER);
        lblText1.setFont(new Font("Tahoma", Font.ITALIC, 14));
        lblText1.setBounds(240, 30, 175, 100);
        panel.add(lblText1);

        return panel;

    }
}
