/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mkyong.swing;

/**
 *
 * @author Francesco Ciappa <f.ciappa@gmail.com>
 */
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class ConfirmDialog1b {
    
    public ConfirmDialog1b() {
        JCheckBox check = new JCheckBox("Tick me");
        Object[] options = {'e', 2, 3.14, 4, 5, "TURTLES!", check};
        int x = JOptionPane.showOptionDialog(null, "So many options using Object[]",
                "Don't forget to Tick it!",
                JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);

        if (check.isSelected() && x != -1) {
            log.info("Your choice was [{}]", options[x]);
        } else {
            log.info(":( no choice");
        }
    }
}
