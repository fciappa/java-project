package com.mkyong;

import org.apache.commons.codec.digest.DigestUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Hashing {
    private static String parameterType;
    private static String parameter;
    
    public Hashing(String param) {
        parameterType = "String";
        parameter = param;
    }
    
    public Hashing(int param) {
        parameterType = "Number";
        parameter = Integer.toString(param);
    }
            
    public void getLog() {
        log.debug("{} provided: [{}]", parameterType, parameter);
        log.debug("Hashed value sha1: [{}]", sha1(parameter));
        log.debug("Hashed value sha256hex: [{}]", sha256hex(parameter));
    }
    
    public static String sha1(String input) {
        return DigestUtils.sha1Hex(input);
    }
    
    public static String sha256hex(String input) {
        return DigestUtils.sha256Hex(input);
    }
}
