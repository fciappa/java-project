/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mkyong;

import com.mkyong.utils.Sleep;
import java.util.concurrent.Semaphore;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Francesco Ciappa <f.ciappa@gmail.com>
 */
@Slf4j
public class SemaphoreTest {
    // max 4 people
    static Semaphore semaphore = new Semaphore(4);
    static class MyATMThread extends Thread {
        String name = "";
        MyATMThread(String name) {
            this.name = name;
        }
        
        public void run() {
            try {
                log.info("[{}] : acquiring lock...", name);
                log.info("[{}] : available Semaphore permits now: [{}]", name, semaphore.availablePermits());

                semaphore.acquire();
                log.info("[{}] : got the permit!", name);
                try {
                    for (int i = 1; i <= 5; i++) {
                        log.info("[{}] : is performing operation [{}], available Semaphore permits : [{}]", name, i, semaphore.availablePermits());
                        // sleep 1 second
                        Sleep.pause(Sleep.ONESECOND, "MyATMThread");
                    }
                } finally {
                    // calling release() after a successful acquire()
                    log.info("[{}] : releasing lock...", name);
                    semaphore.release();
                    log.info("[{}] : available Semaphore permits now: [{}]", name, semaphore.availablePermits());
                }
            } catch (InterruptedException e) {
                log.error("Error on thread", e);
                Thread.currentThread().interrupt();
            }
        }
    }
}
