/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mkyong;

import java.sql.*;
import java.util.Arrays;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Francesco Ciappa <f.ciappa@gmail.com>
 */
@Slf4j
public class JDBCConnection {

    public JDBCConnection() {
        log.info("-------- MySQL JDBC Connection Testing ------------");

        try {
            //Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            log.error("Where is your MySQL JDBC Driver? ", e);
            return;
        }

        String parameter = Arrays.asList("name=?", "unique_label=?", "host=?", "channel=?", "port=?", "username=?", "password=?", "TYPE=?").stream().collect(Collectors.joining(", "));
        String sqlTclManager = "UPDATE TCL_QMANAGER SET " + parameter + " WHERE TYPE=?";
        
        
        String pass = "root";
        log.info("Oracle JDBC Driver Registered!");
        //connection = DriverManager.getConnection("jdbc:mysql://localhost:3306:db.at", "root", "root");
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db.java", "root", pass)) {
            
            log.info("You made it, take control your database now!");
            String sql = "SELECT * FROM test WHERE password=?";
            try (PreparedStatement ps = connection.prepareStatement(sqlTclManager)) {
                ps.setString(1, "prova");
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        log.info(rs.getString("id"));
                    }
                }
            }
        } catch (SQLException e) {
            log.error("Connection Failed! Check output console ", e);
            return;
        }

        
    }
}
