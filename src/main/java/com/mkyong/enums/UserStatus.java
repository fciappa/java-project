/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mkyong.enums;

/**
 *
 * @author Francesco Ciappa <f.ciappa@gmail.com>
 */
public enum UserStatus {
    PENDING,
    ACTIVE,
    INACTIVE,
    DELETED;
}
