/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mkyong.utils;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Francesco Ciappa <f.ciappa@gmail.com>
 */
@Slf4j
public class Sleep {
    public static final long FIFTYMILLIS = 100;
    public static final long ONEHUNDREDMILLIS = 100;
    public static final long HALFSECOND = 500;
    public static final long ONESECOND = 1000;
    public static final long TWOSECONDS = 2000;
    public static final long FIVESECONDS = 5000;
    public static final long TENSECONDS = 10000;
    public static final long SIXTYFIVESECONDS = 65000;

    public static final void pause(final long millis, final String description) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            log.error("Error on: [{}]",description, e);
            Thread.currentThread().interrupt();
        }
    }
}
