package com.mkyong.ftptojms;

import javax.jms.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;

@Slf4j
public class FtpToJMSExample {

    private boolean loaded = false;
    // il valore della variabile DEFAULT_BROKER_URL è: tcp://127.0.0.1:61616
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;
    //private static String ftpLocation = "ftp://192.168.1.102/incoming?username=brutocaterano&password=asterix111";
    private static String ftpLocation = "ftp://127.0.0.1/incoming?username=fciappa&password=frci2609";

    public void start() throws Exception {
        // Istanziamo il Context di Camel e la Connectionfactory per creare la connection ad ActiveMQ utilizzando la variabile url.
        CamelContext context = new DefaultCamelContext();
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        // aggiungiamo un COMPONENTE di Camel che si occupa di creare il produce e l’endPoint verso una coda JMS.
        context.addComponent("jms", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));

        RouteBuilder builder;
        //context.addRoutes(
        builder = new RouteBuilder() {
            public void configure() {
                // creare un instradamento (routes)  da (from) ftpLocation alla (to) coda ‘MYTESTQUEUE‘
                // se non viene trovata una queue con questo nome dal broker semplicemente la creerà
                from(ftpLocation)
                        .process(executeFirstProcessor())
                        .to("jms:MYTESTQUEUE");
            }
        };

        log.info("Start Camel context");
        log.info("Loaded [{}] => Waiting...", loaded);
        context.start();
        context.addRoutes(builder);
        while (loaded != true) {
            log.info("Loaded [{}] => Waiting...", loaded);

        }
        context.stop();
        //context.stopRoute(url);
        log.info("Stop Camel context");
    }

    /*
    public static void main(String args[]) throws Exception {
        FtpToJMSExample j = new FtpToJMSExample();
        j.start();
    }
     */
    private Processor executeFirstProcessor() {
        return new Processor() {
            @Override
            public void process(Exchange exchange) {
                log.info(" We just downloaded: [{}]", exchange.getIn().getHeader("CamelFileName"));
                loaded = true;
            }
        };
    }
}
